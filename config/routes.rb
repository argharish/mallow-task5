Rails.application.routes.draw do
  get 'post_controller/show' => 'post_controller#show'

  get 'post_controller/create' => 'post_controller#create'

  get 'post_controller/update' => 'post_controller#update'

  get 'post_controller/delete' => 'post_controller#delete'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'post_controller#show'

end
