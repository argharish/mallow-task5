require 'test_helper'

class PostControllerControllerTest < ActionDispatch::IntegrationTest
  test "should get show" do
    get post_controller_show_url
    assert_response :success
  end

  test "should get create" do
    get post_controller_create_url
    assert_response :success
  end

  test "should get update" do
    get post_controller_update_url
    assert_response :success
  end

  test "should get delete" do
    get post_controller_delete_url
    assert_response :success
  end

end
